package testy

import (
	"io"
	"os"
	"path/filepath"
)

// copyDir recursively copies source to target. The target will be created if
// it does not exist.
//
// Permissions are not preserved. Symlinks are copied as files.
func copyDir(target, source string) error {
	entries, err := os.ReadDir(source)
	if err != nil {
		return err
	}
	if err := os.MkdirAll(target, tempDirPerms); err != nil && !os.IsExist(err) {
		return err
	}
	for _, entry := range entries {
		sourcePath := filepath.Join(source, entry.Name())
		targetPath := filepath.Join(target, entry.Name())

		if entry.IsDir() {
			if err := os.MkdirAll(targetPath, tempDirPerms); err != nil && !os.IsExist(err) {
				return err
			}
			if err := copyDir(targetPath, sourcePath); err != nil {
				return err
			}
			continue
		}
		if err := copyFile(targetPath, sourcePath); err != nil {
			return err
		}
	}
	return nil
}

func copyFile(target, source string) error {
	out, err := os.Create(target)
	if err != nil {
		return err
	}
	defer out.Close()

	in, err := os.Open(source)
	if err != nil {
		return err
	}
	defer in.Close()

	_, err = io.Copy(out, in)
	return err
}
