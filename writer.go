package testy

import (
	"bytes"
	"errors"
	"io"
	"sync"
)

type errWriter struct {
	count int
	err   error
}

var _ io.WriteCloser = &errWriter{}

func (w *errWriter) Write(p []byte) (int, error) {
	c := len(p)
	if c >= w.count {
		return w.count, w.err
	}
	w.count -= c
	return c, nil
}

func (w *errWriter) Close() error { return w.err }

// ErrorWriter returns a new io.Writer, which will accept count bytes
// (discarding them), then return err as an error. A call to Close() will
// also return err.
func ErrorWriter(count int, err error) io.WriteCloser {
	return &errWriter{count: count, err: err}
}

type recorder struct {
	*bytes.Buffer

	mu *sync.Mutex
	// closed state
	// open:   available for writing
	// closed: closed for writing, available for reading
	// nil:    closed for writing and reading
	closed chan struct{}
}

var _ io.ReadWriteCloser = &recorder{}

const (
	writeMode = iota
	readMode
	closed
)

// currentMode returns true if the closed state matches mode.
func (r *recorder) currentMode(mode int) bool {
	r.mu.Lock()
	defer r.mu.Unlock()
	if r.closed == nil {
		return closed == mode
	}
	select {
	case <-r.closed:
		return readMode == mode
	default:
		return writeMode == mode
	}
}

func (r *recorder) Write(p []byte) (int, error) {
	if !r.currentMode(writeMode) {
		return 0, errors.New("recorder closed for writing")
	}
	return r.Buffer.Write(p)
}

func (r *recorder) Close() error {
	if r.currentMode(writeMode) {
		close(r.closed)
		return nil
	}
	if r.currentMode(closed) {
		return errors.New("already closed")
	}
	r.mu.Lock()
	r.closed = nil
	r.mu.Unlock()
	return nil
}

func (r *recorder) Read(p []byte) (int, error) {
	if r.currentMode(writeMode) {
		<-r.closed
	}
	if !r.currentMode(readMode) {
		return 0, errors.New("recorder closed")
	}
	return r.Buffer.Read(p)
}

// Recorder returns an io.ReadWriteCloser, which can be used for testing writes.
// The Read() method will block until Close() is called the first time, at which
// point any further Write() calls will fail, and any data previously written
// will be read back. A second call to Close() clears the buffer and will cause
// further Read() calls to fail as well.
func Recorder() io.ReadWriteCloser {
	return &recorder{
		Buffer: &bytes.Buffer{},
		mu:     &sync.Mutex{},
		closed: make(chan struct{}),
	}
}
