//go:build go1.21

package testy

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSlogLines(t *testing.T) {
	logger, buf := NewSlog()
	logger.Debug("debug")
	logger.Info("info")
	logger.Info("keys", "cow", "moo")

	want := []string{
		`level=DEBUG msg=debug`,
		`level=INFO msg=info`,
		`level=INFO msg=keys cow=moo`,
	}
	if d := cmp.Diff(want, buf.Lines()); d != "" {
		t.Errorf("unexpected log output (-want +got):\n%s", d)
	}
}

func TestSlogMatches(t *testing.T) {
	logger, buf := NewSlog()
	logger.Debug("debug")
	logger.Info("info")
	logger.Info("keys", "cow", "moo")

	t.Run("success", func(t *testing.T) {
		want := []string{
			`level=DEBUG msg=debug`,
			`level=INFO msg=info`,
			`level=INFO msg=keys cow=moo`,
		}
		if err := buf.Matches(want); err != nil {
			t.Error(err)
		}
	})

	t.Run("fail", func(t *testing.T) {
		want := []string{
			`level=DEBUG msg=debug`,
			`level=INFO msg=info`,
		}
		err := buf.Matches(want)
		if !strings.Contains(err.Error(), "count mismatch") {
			t.Error(err)
		}
	})
}

func TestSlogMatchesRE(t *testing.T) {
	logger, buf := NewSlog()
	logger.Debug("debug")
	logger.Info("info")
	logger.Info("keys", "cow", "moo")

	t.Run("success", func(t *testing.T) {
		want := []string{
			`level=DEBUG msg=.*`,
			`level=(INFO|ERROR) msg=info`,
			`level=INFO msg=keys cow=moo`,
		}
		if err := buf.MatchesRE(want); err != nil {
			t.Error(err)
		}
	})

	t.Run("fail", func(t *testing.T) {
		want := []string{
			`level=DEBUG msg=debug`,
			`level=INFO msg=info`,
		}
		err := buf.MatchesRE(want)
		if !strings.Contains(err.Error(), "count mismatch") {
			t.Error(err)
		}
	})
}
