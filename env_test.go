package testy

import (
	"os"
	"sort"
	"testing"
)

func TestRestoreEnv(t *testing.T) {
	os.Clearenv()
	_ = os.Setenv("foo", "bar")
	_ = os.Setenv("bar", "baz")

	func() {
		defer RestoreEnv()()
		if err := os.Setenv("baz", "qux"); err != nil {
			t.Fatal(err)
		}
		if err := os.Unsetenv("bar"); err != nil {
			t.Fatal(err)
		}
		env := os.Environ()
		expected := []string{"foo=bar", "baz=qux"}
		if d := DiffInterface(expected, env); d != nil {
			t.Fatal(d)
		}
	}()

	env := os.Environ()
	expected := []string{"foo=bar", "bar=baz"}
	sort.Strings(expected)
	sort.Strings(env)
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}

func TestEnviron(t *testing.T) {
	os.Clearenv()
	if err := os.Setenv("foo", "bar"); err != nil {
		t.Fatal(err)
	}
	if err := os.Setenv("bar", "baz"); err != nil {
		t.Fatal(err)
	}
	expected := map[string]string{
		"foo": "bar",
		"bar": "baz",
	}
	env := Environ()
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}

func TestSetEnv(t *testing.T) {
	os.Clearenv()
	if err := os.Setenv("foo", "bar"); err != nil {
		t.Fatal(err)
	}

	_ = SetEnv(map[string]string{
		"bar": "baz",
		"baz": "qux",
	})

	expected := map[string]string{
		"foo": "bar",
		"bar": "baz",
		"baz": "qux",
	}
	env := Environ()
	if d := DiffInterface(expected, env); d != nil {
		t.Fatal(d)
	}
}
