//go:build go1.21

package testy

import (
	"bytes"
	"errors"
	"fmt"
	"log/slog"
	"regexp"
	"strings"
)

// Log contains a buffer handle for making assertions on logs.
type Log struct {
	*bytes.Buffer
}

// NewSlog returns a new [log/slog.Logger] instance which logs in text format,
// and a buffer handle, which can be used for making assertions. The date/time
// of each log is removed for easier assertions.
func NewSlog() (*slog.Logger, Log) {
	var buf bytes.Buffer
	return slog.New(slog.NewTextHandler(&buf, slogOptions())), Log{&buf}
}

// NewJSONSlog returns is the same as [NewSlog], but formats logs as JSON.
func NewJSONSlog() (*slog.Logger, Log) {
	var buf bytes.Buffer
	return slog.New(slog.NewJSONHandler(&buf, slogOptions())), Log{&buf}
}

func slogOptions() *slog.HandlerOptions {
	return &slog.HandlerOptions{
		Level: slog.LevelDebug,
		ReplaceAttr: func(_ []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		},
	}
}

// String returns the full log buffer as a string.
func (l *Log) String() string {
	return l.Buffer.String()
}

// Lines returns the log buffer as a slice of lines.
func (l *Log) Lines() []string {
	buf := l.Buffer.Bytes()
	buf = bytes.ReplaceAll(buf, []byte("\r\n"), []byte("\n"))
	buf = bytes.TrimRight(buf, "\n")

	sliced := bytes.Split(buf, []byte("\n"))
	result := make([]string, len(sliced))
	for i, line := range sliced {
		result[i] = string(line)
	}
	return result
}

// Matches does a line-by-line comparison of the log buffer with the given lines.
// Leading and trailing whitespace is ignored. It returns an error if the logs
// do not match expectations.
func (l *Log) Matches(want []string) error {
	var errs []error
	got := l.Lines()
	for i := range got {
		if i > len(want)-1 {
			break
		}
		g := strings.TrimSpace(got[i])
		w := strings.TrimSpace(want[i])
		if g != w {
			errs = append(errs, fmt.Errorf("Log line %d mismatch: got %q, want %q", i+1, g, w))
		}
	}
	if len(got) != len(want) {
		errs = append(errs, fmt.Errorf("Log lines count mismatch: got %d, want %d", len(got), len(want)))
	}
	return errors.Join(errs...)
}

// MatchesRE does a line-by-line comparison of the log buffer with the given
// regular expressions. It returns an error if the logs do not match
// expectations. It will panic if passed an invalid regular expression.
func (l *Log) MatchesRE(want []string) error {
	var errs []error
	got := l.Lines()
	for i := range got {
		if i > len(want)-1 {
			break
		}
		re := regexp.MustCompile(want[i])
		if !re.MatchString(got[i]) {
			errs = append(errs, fmt.Errorf("Log line %d mismatch: got %q, want /%s/", i+1, got[i], want[i]))
		}
	}
	if len(got) != len(want) {
		errs = append(errs, fmt.Errorf("Log lines count mismatch: got %d, want %d", len(got), len(want)))
	}
	return errors.Join(errs...)
}
