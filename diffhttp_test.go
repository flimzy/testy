package testy

import (
	"bufio"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
)

func TestZeroDate(t *testing.T) {
	t.Run("none", func(t *testing.T) {
		if ZeroDate(http.Header{}) {
			t.Error("Should be false")
		}
	})
	t.Run("date", func(t *testing.T) {
		h := http.Header{
			"Date": []string{"foo"},
		}
		if !ZeroDate(h) {
			t.Error("Should be true")
		}
		want := "Thu, 01 Jan 1970 00:00:00 UTC"
		if got := h.Get("Date"); got != want {
			t.Errorf("Want %s, got %s", want, got)
		}
	})
}

func TestHTTPRequest(t *testing.T) {
	type tt struct {
		expected, actual interface{}
		re               []Replacement
		result           string
	}

	tests := NewTable()
	tests.Add("two GET requests", tt{
		expected: httptest.NewRequest("GET", "/", nil),
		actual:   httptest.NewRequest("GET", "/", nil),
	})
	tests.Add("missing snapshot", tt{
		expected: &File{Path: "/this/doesnt/exist"},
		actual:   httptest.NewRequest("GET", "/foo.html", nil),
		result:   "To regenerate the on-disk snapshot, re-run the tests with `UPDATE_TESTY=true` or `-update-testy` or `-tags=update`.\n--- expected\n+++ actual\n@@ -1 +1,3 @@\n-\n+GET /foo.html HTTP/1.1\r\n+Host: example.com\r\n+\r\n",
	})
	tests.Add("different URLs requests", tt{
		expected: httptest.NewRequest("GET", "/", nil),
		actual:   httptest.NewRequest("GET", "/foo.html", nil),
		result:   "--- expected\n+++ actual\n@@ -1,3 +1,3 @@\n-GET / HTTP/1.1\r\n+GET /foo.html HTTP/1.1\r\n Host: example.com\r\n \r\n",
	})
	tests.Add("nil request", tt{
		expected: nil,
		actual:   httptest.NewRequest("GET", "/foo.html", nil),
		result:   "--- expected\n+++ actual\n@@ -1 +1,3 @@\n-\n+GET /foo.html HTTP/1.1\r\n+Host: example.com\r\n+\r\n",
	})
	tests.Add("string", tt{
		expected: `GET / HTTP/1.1
Host: localhost:6005
User-Agent: curl/7.52.1
Accept: */*

`,
		actual: &http.Request{
			Method:     http.MethodGet,
			ProtoMajor: 1,
			ProtoMinor: 1,
			URL:        &url.URL{Host: "localhost:6005"},
			Header: http.Header{
				"Accept":     []string{"*/*"},
				"User-Agent": []string{"curl/7.52.1"},
			},
		},
	})
	tests.Add("byte slice", tt{
		expected: []byte(`GET / HTTP/1.1
Host: localhost:6005
User-Agent: curl/7.52.1
Accept: */*

`),
		actual: &http.Request{
			Method:     http.MethodGet,
			ProtoMajor: 1,
			ProtoMinor: 1,
			URL:        &url.URL{Host: "localhost:6005"},
			Header: http.Header{
				"Accept":     []string{"*/*"},
				"User-Agent": []string{"curl/7.52.1"},
			},
		},
	})
	tests.Add("replacement", tt{
		expected: []byte(`GET / HTTP/1.1
Host: tsohlacol:6005
User-Agent: curl/7.52.1
Accept: */*

`),
		actual: &http.Request{
			Method:     http.MethodGet,
			ProtoMajor: 1,
			ProtoMinor: 1,
			URL:        &url.URL{Host: "localhost:6005"},
			Header: http.Header{
				"Accept":     []string{"*/*"},
				"User-Agent": []string{"curl/7.52.1"},
			},
		},
		re: []Replacement{
			{
				Regexp:      regexp.MustCompile("localhost"),
				Replacement: "tsohlacol",
			},
		},
	})
	tests.Add("file", tt{
		expected: &File{Path: "testdata/request.raw"},
		actual: &http.Request{
			Method:     http.MethodGet,
			ProtoMajor: 1,
			ProtoMinor: 1,
			URL:        &url.URL{Host: "localhost:6005"},
			Header: http.Header{
				"Accept":     []string{"*/*"},
				"User-Agent": []string{"curl/7.52.1"},
			},
		},
	})
	tests.Add("nil", tt{
		expected: nil,
		actual:   nil,
	})
	tests.Add("unknown input type", tt{
		expected: 123,
		actual:   nil,
		result:   "Failed to dump expected request: Unable to convert int to *http.Request",
	})
	tests.Add("nil body", func(t *testing.T) interface{} {
		req := httptest.NewRequest("PUT", "/foo", nil)
		req.Body = nil

		return tt{
			expected: Snapshot(t),
			actual:   req,
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := DiffHTTPRequest(tt.expected, tt.actual, tt.re...)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if d := DiffText(tt.result, resultText); d != nil {
			t.Error(d)
		}
	})
}

func TestHTTPResponse(t *testing.T) {
	type tt struct {
		expected, actual interface{}
		re               []Replacement
		result           string
	}

	tests := NewTable()

	tests.Add("two empty responses", tt{
		expected: &http.Response{},
		actual:   &http.Response{},
	})
	tests.Add("missing snapshot", tt{
		expected: &File{Path: "/this/doesnt/exist"}, // This file must not exist
		actual: &http.Response{
			Header: http.Header{"Foo": []string{"qux"}},
		},
		result: "To regenerate the on-disk snapshot, re-run the tests with `UPDATE_TESTY=true` or `-update-testy` or `-tags=update`.\n--- expected\n+++ actual\n@@ -1 +1,3 @@\n-\n+HTTP/0.0 000 status code 0\r\n+Foo: qux\r\n+\r\n",
	})
	tests.Add("Different headers", tt{
		expected: &http.Response{
			Header: http.Header{"Foo": []string{"bar"}},
		},
		actual: &http.Response{
			Header: http.Header{"Foo": []string{"qux"}},
		},
		result: "--- expected\n+++ actual\n@@ -1,3 +1,3 @@\n HTTP/0.0 000 status code 0\r\n-Foo: bar\r\n+Foo: qux\r\n \r\n",
	})
	tests.Add("nil response body", tt{
		expected: nil,
		actual: &http.Response{
			Header: http.Header{"Foo": []string{"qux"}},
		},
		result: "--- expected\n+++ actual\n@@ -1 +1,3 @@\n-\n+HTTP/0.0 000 status code 0\r\n+Foo: qux\r\n+\r\n",
	})
	tests.Add("two nil bodies", tt{
		expected: &http.Response{
			StatusCode: 400,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header:     http.Header{"Foo": []string{"qux"}},
		},
		actual: &http.Response{
			StatusCode: 400,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header:     http.Header{"Foo": []string{"qux"}},
		},
	})
	tests.Add("read from file", tt{
		expected: &File{Path: "testdata/response.raw"},
		actual: &http.Response{
			StatusCode:    http.StatusOK,
			ProtoMajor:    1,
			ProtoMinor:    1,
			ContentLength: 11,
			Header: http.Header{
				"Content-Type": []string{"application/json"},
				"Date":         []string{"Tue, 22 Jan 2019 18:44:09 GMT"},
			},
			Body: io.NopCloser(strings.NewReader(`{"ok":true}`)),
		},
	})
	tests.Add("string", tt{
		expected: `HTTP/1.1 200 OK
Content-Length: 11
Content-Type: application/json
Date: Tue, 22 Jan 2019 18:44:09 GMT

{"ok":true}
`,
		actual: &http.Response{
			StatusCode:    http.StatusOK,
			ProtoMajor:    1,
			ProtoMinor:    1,
			ContentLength: 11,
			Header: http.Header{
				"Content-Type": []string{"application/json"},
				"Date":         []string{"Tue, 22 Jan 2019 18:44:09 GMT"},
			},
			Body: io.NopCloser(strings.NewReader(`{"ok":true}`)),
		},
	})
	tests.Add("byte slice", tt{
		expected: []byte(`HTTP/1.1 200 OK
Content-Length: 11
Content-Type: application/json
Date: Tue, 22 Jan 2019 18:44:09 GMT

{"ok":true}
`),
		actual: &http.Response{
			StatusCode:    http.StatusOK,
			ProtoMajor:    1,
			ProtoMinor:    1,
			ContentLength: 11,
			Header: http.Header{
				"Content-Type": []string{"application/json"},
				"Date":         []string{"Tue, 22 Jan 2019 18:44:09 GMT"},
			},
			Body: io.NopCloser(strings.NewReader(`{"ok":true}`)),
		},
	})
	tests.Add("replacement", tt{
		expected: []byte(`HTTP/1.1 200 OK
Content-Length: 11
Content-Type: application/nosj
Date: Tue, 22 Jan 2019 18:44:09 GMT

{"ok":true}
`),
		actual: &http.Response{
			StatusCode:    http.StatusOK,
			ProtoMajor:    1,
			ProtoMinor:    1,
			ContentLength: 11,
			Header: http.Header{
				"Content-Type": []string{"application/json"},
				"Date":         []string{"Tue, 22 Jan 2019 18:44:09 GMT"},
			},
			Body: io.NopCloser(strings.NewReader(`{"ok":true}`)),
		},
		re: []Replacement{
			{
				Regexp:      regexp.MustCompile(`json`),
				Replacement: `nosj`,
			},
		},
	})
	tests.Add("unknown input type", tt{
		expected: int(123),
		result:   "Failed to dump expected response: Unable to convert int to *http.Response",
	})
	tests.Add("content-length", func(t *testing.T) interface{} {
		r := strings.NewReader(`HTTP/1.1 200 OK
Content-Length: 0
Date: Thu, 01 Jan 1970 00:00:00 UTC

`)
		exp, err := http.ReadResponse(bufio.NewReader(r), nil)
		if err != nil {
			t.Fatal(err)
		}

		e := echo.New()
		e.POST("/", func(c echo.Context) error {
			return c.NoContent(http.StatusOK)
		})

		s := httptest.NewServer(e)
		t.Cleanup(s.Close)

		req, _ := http.NewRequest(http.MethodPost, s.URL, strings.NewReader("foo"))
		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}

		ZeroDate(res.Header)

		return tt{
			expected: exp,
			actual:   res,
		}
	})
	tests.Add("synthetic vs test responses", func(*testing.T) interface{} {
		rec := httptest.NewRecorder()
		return tt{
			expected: &http.Response{
				ProtoMajor: 1,
				ProtoMinor: 1,
				StatusCode: http.StatusOK,
			},
			actual: rec.Result(),
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := DiffHTTPResponse(tt.expected, tt.actual, tt.re...)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if d := DiffText(tt.result, resultText); d != nil {
			t.Error(d)
		}
	})
}

func Test_dumpRequest(t *testing.T) {
	type tt struct {
		req interface{}
		err string
	}

	tests := NewTable()
	tests.Add("invalid request", tt{
		req: 123,
		err: "Unable to convert int to *http.Request",
	})
	tests.Add("do not add Content-Length", tt{
		req: httptest.NewRequest(http.MethodPost, "/", strings.NewReader("foo")),
	})
	tests.Add("add Content-Length", tt{
		req: httptest.NewRequest(http.MethodGet, "/", nil),
	})
	tests.Add("add 0 Content-Length", tt{
		req: httptest.NewRequest(http.MethodPost, "/", nil),
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := dumpRequest(tt.req)
		Error(t, tt.err, err)
		if d := DiffInterface(Snapshot(t), got); d != nil {
			t.Error(d)
		}
	})
}
