package testy

import "testing"

func TestDiffURLQuery(t *testing.T) {
	type tt struct {
		expected, actual interface{}
	}

	tests := NewTable()
	tests.Add("both empty", tt{})
	tests.Add("want empty", tt{
		actual: "foo=bar",
	})
	tests.Add("got empty", tt{
		expected: "foo=bar",
	})
	tests.Add("different order", tt{
		expected: "foo=bar&bar=baz",
		actual:   "bar=baz&foo=bar",
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := DiffURLQuery(tt.expected, tt.actual)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if d := DiffText(Snapshot(t), resultText); d != nil {
			t.Error(d)
		}
	})
}
