package testy

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"sort"
	"strconv"
	"strings"
	"time"
)

// ZeroDate sets the Date header, if found, to January 1, 1970. It returns false
// if the Date header was not found.
func ZeroDate(h http.Header) bool {
	if _, ok := h["Date"]; !ok {
		return false
	}
	h.Set("Date", time.Unix(0, 0).UTC().Format(time.RFC1123))
	return true
}

// DiffHTTPRequest compares the metadata and bodies of the two HTTP requests,
// and returns the difference.
// Inputs must be of the type *http.Request, or of one of the following types,
// in which case the input is interpreted as a raw HTTP request.
// - io.Reader
// - string
// - []byte
//
// If the actual request does not contain a Content-Length header or
// Transfer-Encoding: chunked, and it should contain one, then Content-Length
// is added, so that reading the request will succeed.
func DiffHTTPRequest(expected, actual interface{}, re ...Replacement) *Diff {
	expDump, expErr := dumpRequest(expected)
	actDump, err := dumpRequest(actual)
	act := replace(string(actDump), re...)
	if err != nil {
		return &Diff{err: fmt.Sprintf("Failed to dump actual request: %s", err)}
	}
	var d *Diff
	if expErr != nil {
		d = &Diff{err: fmt.Sprintf("Failed to dump expected request: %s", expErr)}
	} else {
		d = DiffText(string(expDump), act)
	}
	return update(UpdateMode(), expected, act, d)
}

func toRequest(i interface{}) (*http.Request, error) {
	var r io.Reader
	switch t := i.(type) {
	case *http.Request:
		return t, nil
	case io.Reader:
		r = t
	case string:
		r = strings.NewReader(t)
	case []byte:
		r = bytes.NewReader(t)
	default:
		return nil, fmt.Errorf("Unable to convert %T to *http.Request", i)
	}
	br := bufio.NewReader(r)
	if _, err := br.ReadByte(); err != nil {
		return nil, err
	}
	if err := br.UnreadByte(); err != nil {
		return nil, err
	}
	return http.ReadRequest(br)
}

func dumpRequest(i interface{}) ([]byte, error) {
	if i == nil {
		return nil, nil
	}
	req, err := toRequest(i)
	if err == io.EOF {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	if shouldAddLength(req) {
		body, err := io.ReadAll(req.Body)
		if err != nil {
			return nil, err
		}
		if err := req.Body.Close(); err != nil {
			return nil, err
		}
		req.Header.Add("Content-Length", strconv.Itoa(len(body)))
		req.Body = io.NopCloser(bytes.NewReader(body))
	}
	return httputil.DumpRequest(req, true)
}

func shouldAddLength(r *http.Request) bool {
	if r.Method == http.MethodGet {
		return false
	}
	if r.Body == nil {
		r.Body = io.NopCloser(strings.NewReader(""))
	}
	var v string
	if v = r.Header.Get("Content-Length"); v != "" {
		return false
	}
	if v = r.Header.Get("Transfer-Encoding"); v == "chunked" {
		return false
	}
	return true
}

// DiffHTTPResponse compares the metadata and bodies of the two HTTP responses,
// and returns the difference.
// Inputs must be of the type *http.Response, or of one of the following types,
// in which case the input is interpreted as a raw HTTP response.
// - io.Reader
// - string
// - []byte
func DiffHTTPResponse(expected, actual interface{}, re ...Replacement) *Diff {
	expDump, expErr := dumpResponse(expected)
	actDump, err := dumpResponse(actual)
	act := replace(string(actDump), re...)
	if err != nil {
		return &Diff{err: fmt.Sprintf("Failed to dump actual response: %s", err)}
	}
	var d *Diff
	if expErr != nil {
		d = &Diff{err: fmt.Sprintf("Failed to dump expected response: %s", expErr)}
	} else {
		d = DiffText(string(expDump), act)
	}
	return update(UpdateMode(), expected, act, d)
}

func toResponse(i interface{}) (*http.Response, error) {
	var r io.Reader
	switch t := i.(type) {
	case *http.Response:
		return t, nil
	case io.Reader:
		r = t
	case string:
		r = strings.NewReader(t)
	case []byte:
		r = bytes.NewReader(t)
	default:
		return nil, fmt.Errorf("Unable to convert %T to *http.Response", i)
	}
	br := bufio.NewReader(r)
	if _, err := br.ReadByte(); err != nil {
		return nil, err
	}
	if err := br.UnreadByte(); err != nil {
		return nil, err
	}
	return http.ReadResponse(br, nil)
}

func dumpResponse(i interface{}) ([]byte, error) {
	if i == nil {
		return nil, nil
	}
	res, err := toResponse(i)
	if err == io.EOF {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	connClose := res.Header.Get("Connection") == "close"
	clZero := res.Header.Get("Content-Length") == "0"
	result, err := httputil.DumpResponse(res, true)
	if err != nil {
		return nil, err
	}
	lines := bytes.Split(result, []byte("\r\n"))
	header := make([][]byte, 0, len(lines))
	for _, line := range lines[1:] {
		if len(line) <= 1 {
			break
		}
		if (!connClose && string(line) == "Connection: close") ||
			(!clZero && string(line) == "Content-Length: 0") {
			lines = append(lines[:1], lines[2:]...) // Remove one header line from the final output
			continue
		}
		header = append(header, line)
	}

	sort.Slice(header, func(i, j int) bool {
		return bytes.Compare(header[i], header[j]) < 0
	})
	copy(lines[1:], header)
	return bytes.Join(lines, []byte("\r\n")), nil
}
