package testy

import (
	"fmt"
	"reflect"
	"testing"
)

// generator should return a test, and an optional cleanup func.
type generator func(*testing.T) interface{}

// Table is a table of one or more tests to run against a single test function.
type Table struct {
	gens map[string]generator
}

// NewTable returns a new Table instance.
func NewTable() *Table {
	return &Table{
		gens: make(map[string]generator),
	}
}

// Add adds a single named test to the table, and optionally one or more
// cleanup functions. The cleanup function may also be returned by the test
// generator, if that is easier for scoping of closures.
//
// test may be of the following types:
//
//   - interface{}
//   - func() interface{}
//   - func(*testing.T) interface{}
//   - func() (interface{}, CleanupFunc)
//   - func(*testing.T) (interface{}, CleanupFunc)
//
// If multiple cleanup functions are provided, first the one returned by the
// generator is executed, then any passed to Add() are executed, in the order
// provided.
func (tb *Table) Add(name string, test interface{}) {
	if _, ok := tb.gens[name]; ok {
		panic("Add(): Test " + name + " already defined.")
	}

	var gen generator
	switch typedTest := test.(type) {
	case func() interface{}:
		gen = func(*testing.T) interface{} {
			return typedTest()
		}
	case func(*testing.T) interface{}:
		gen = func(t *testing.T) interface{} {
			return typedTest(t)
		}
	default:
		if reflect.TypeOf(test).Kind() == reflect.Func {
			panic(fmt.Sprintf("Test generator must be of type func(*testing.T) interface{}, or func() interface{}. Got %T", test))
		}
		gen = func(*testing.T) interface{} {
			return test
		}
	}
	tb.gens[name] = gen
}

// Run cycles through the defined tests, passing them one at a time to testFn.
// testFn must be a function which takes two arguments: *testing.T, and an
// arbitrary type, which must match the return value of the Generator functions.
func (tb *Table) Run(t *testing.T, testFn interface{}) {
	testFnT := reflect.TypeOf(testFn)
	if testFnT.Kind() != reflect.Func {
		panic("testFn must be a function")
	}
	if testFnT.NumIn() != 2 || testFnT.In(0) != reflect.TypeOf(&testing.T{}) {
		panic("testFn must be of the form func(*testing.T, **)")
	}
	testType := reflect.TypeOf(testFn).In(1)
	testFnV := reflect.ValueOf(testFn)
	for name, genFn := range tb.gens {
		t.Run(name, func(t *testing.T) {
			t.Helper()
			test := genFn(t)
			if reflect.TypeOf(test) != testType {
				t.Fatalf("Test generator returned wrong type. Have %T, want %s", test, testType.Name())
			}
			_ = testFnV.Call([]reflect.Value{reflect.ValueOf(t), reflect.ValueOf(test)})
		})
	}
}
