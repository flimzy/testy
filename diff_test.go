package testy

import (
	"encoding/json"
	"errors"
	"io"
	"regexp"
	"strings"
	"testing"
)

func TestResultString(t *testing.T) {
	t.Run("nil", func(t *testing.T) {
		var r *Diff
		expected := ""
		if result := r.String(); result != expected {
			t.Errorf("Unexpected result: %s", result)
		}
	})
	t.Run("diff", func(t *testing.T) {
		expected := "foo"
		r := &Diff{diff: expected}
		if result := r.String(); result != expected {
			t.Errorf("Unexpected result: %s", result)
		}
	})
}

func TestSliceDiff(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual []string
		result           string
	}{
		{
			name:     "equal",
			expected: []string{"foo"},
			actual:   []string{"foo"},
		},
		{
			name:     "different",
			expected: []string{"foo"},
			actual:   []string{"bar"},
			result:   "--- expected\n+++ actual\n@@ -1 +1 @@\n-foo+bar",
		},
	}
	for _, test := range tests {
		result := sliceDiff(test.expected, test.actual)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if resultText != test.result {
			t.Errorf("Unexpected result:\n%s\n", resultText)
		}
	}
}

func TestTextSlices(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual []string
		result           string
	}{
		{
			name:     "equal",
			expected: []string{"foo", "bar"},
			actual:   []string{"foo", "bar"},
		},
		{
			name:     "different",
			expected: []string{"foo", "bar"},
			actual:   []string{"bar", "bar"},
			result:   "--- expected\n+++ actual\n@@ -1,2 +1,2 @@\n-foo\n bar\n+bar\n",
		},
	}
	for _, test := range tests {
		result := DiffTextSlices(test.expected, test.actual)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if resultText != test.result {
			t.Errorf("Unexpected result:\n%s\n", resultText)
		}
	}
}

func TestText(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual interface{}
		result           string
	}{
		{
			name:     "equal",
			expected: "foo\nbar\n",
			actual:   "foo\nbar\n",
		},
		{
			name:     "different",
			expected: "foo\nbar",
			actual:   "bar\nbar",
			result:   "--- expected\n+++ actual\n@@ -1,2 +1,2 @@\n-foo\n bar\n+bar\n",
		},
		{
			name:     "string vs []byte",
			expected: "foo",
			actual:   []byte("foo"),
		},
		{
			name:     "invalid exp type",
			expected: 123,
			result:   "[diff] expected: input must be of type string, []byte, or io.Reader",
		},
		{
			name:     "invalid act type",
			expected: "123",
			actual:   123,
			result:   "[diff] actual: input must be of type string, []byte, or io.Reader",
		},
		{
			name:     "string vs io.Reader",
			expected: strings.NewReader("foo"),
			actual:   "foo",
		},
		{
			name:     "string vs nil",
			expected: "foo",
			actual:   nil,
			result:   "--- expected\n+++ actual\n@@ -1 +1 @@\n-foo\n+\n",
		},
		{
			name:     "file input",
			expected: &File{Path: "testdata/test.txt"},
			actual:   "Test Content\n",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := DiffText(test.expected, test.actual)
			var resultText string
			if result != nil {
				resultText = result.String()
			}
			if resultText != test.result {
				t.Errorf("Unexpected result:\n%s\n", resultText)
			}
		})
	}
}

func TestIsJSON(t *testing.T) {
	tests := []struct {
		name   string
		input  interface{}
		isJSON bool
		result string
	}{
		{
			name:   "io.Reader",
			input:  strings.NewReader("foo"),
			isJSON: true,
			result: "foo",
		},
		{
			name:   "byte slice",
			input:  []byte("foo"),
			isJSON: true,
			result: "foo",
		},
		{
			name:   "json.RawMessage",
			input:  json.RawMessage("foo"),
			isJSON: true,
			result: "foo",
		},
		{
			name:   "string",
			input:  "foo",
			isJSON: false,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			isJSON, result, _ := isJSON(test.input)
			if isJSON != test.isJSON {
				t.Errorf("Unexpected result: %t", isJSON)
			}
			if string(result) != test.result {
				t.Errorf("Unexpected result: %s", string(result))
			}
		})
	}
}

type errorReader struct{}

var _ io.Reader = &errorReader{}

func (r *errorReader) Read(_ []byte) (int, error) {
	return 0, errors.New("read error")
}

func TestMarshal(t *testing.T) {
	tests := []struct {
		name     string
		input    interface{}
		expected string
		err      string
	}{
		{
			name:     "byte slice",
			input:    []byte(`"foo"`),
			expected: `"foo"`,
		},
		{
			name:     "string",
			input:    "foo",
			expected: `"foo"`,
		},
		{
			name:  "invalid json",
			input: []byte("invalid json"),
			err:   "invalid character 'i' looking for beginning of value",
		},
		{
			name:  "error reader",
			input: &errorReader{},
			err:   "read error",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result, err := marshal(test.input)
			var errMsg string
			if err != nil {
				errMsg = err.Error()
			}
			if test.err != errMsg {
				t.Errorf("Unexpected error: %s", errMsg)
			}
			if string(result) != test.expected {
				t.Errorf("Unexpected result: %s", string(result))
			}
		})
	}
}

func TestAsJSON(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual interface{}
		re               Replacement
		result           string
	}{
		{
			name:     "equal",
			expected: []string{"foo", "bar"},
			actual:   []string{"foo", "bar"},
		},
		{
			name:     "different",
			expected: []string{"foo", "bar"},
			actual:   []string{"bar", "bar"},
			result: `--- expected
+++ actual
@@ -1,4 +1,4 @@
 [
-    "foo",
+    "bar",
     "bar"
 ]
`,
		},
		{
			name:     "Unmarshalable expected",
			expected: make(chan int),
			actual:   "foo",
			result:   "failed to marshal expected value: json: unsupported type: chan int",
		},
		{
			name:     "Unmarshalable actual",
			expected: "foo",
			actual:   make(chan int),
			result:   "failed to marshal actual value: json: unsupported type: chan int",
		},
		{
			name:     "empty reader",
			expected: strings.NewReader(""),
			actual:   nil,
			result:   "",
		},
		{
			name:     "regex replacement",
			expected: map[string]string{"foo": "replace"},
			actual:   strings.NewReader(`{"foo":"alternative"}`),
			re: Replacement{
				Regexp:      regexp.MustCompile(`replace`),
				Replacement: "alternative",
			},
		},
		{
			name: "deterministic json",
			expected: struct {
				Foo string `json:"foo"`
				Bar string `json:"bar"`
			}{
				Foo: "foo",
				Bar: "bar",
			},
			actual: map[string]string{
				"foo": "foo",
				"bar": "bar",
				"baz": "baz",
			},
			result: `--- expected
+++ actual
@@ -1,4 +1,5 @@
 {
     "bar": "bar",
+    "baz": "baz",
     "foo": "foo"
 }
`,
		},
	}
	for _, test := range tests {
		var res []Replacement
		if test.re.Regexp != nil {
			res = []Replacement{test.re}
		}
		result := DiffAsJSON(test.expected, test.actual, res...)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if resultText != test.result {
			t.Errorf("Unexpected result:\n%s\n", resultText)
		}
	}
}

func TestJSON(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual interface{}
		result           string
	}{
		{
			name:     "equal strings",
			expected: `["foo","bar"]`,
			actual:   `["foo","bar"]`,
		},
		{
			name:     "equal byte slice",
			expected: []byte(`["foo","bar"]`),
			actual:   `["foo","bar"]`,
		},
		{
			name:     "different",
			expected: `["foo","bar"]`,
			actual:   `["bar","bar"]`,
			result: `--- expected
+++ actual
@@ -1,4 +1,4 @@
 [
-    "foo",
+    "bar",
     "bar"
 ]
`,
		},
		{
			name:     "invalid expected",
			expected: "invalid json",
			actual:   `"foo"`,
			result:   "failed to unmarshal expected value: invalid character 'i' looking for beginning of value",
		},
		{
			name:     "invalid actual",
			expected: `"foo"`,
			actual:   "invalid json",
			result:   "failed to unmarshal actual value: invalid character 'i' looking for beginning of value",
		},
		{
			name:     "empty string",
			expected: "",
			actual:   "",
		},
		{
			name:     "io.Reader",
			expected: strings.NewReader(`["foo","bar"]`),
			actual:   `["foo","bar"]`,
		},
		{
			name:     "empty io.Reader",
			expected: strings.NewReader(``),
			actual:   ``,
		},
		{
			name:     "io.Reader error",
			expected: ErrorReader(``, errors.New("foo")),
			result:   "expected: foo",
		},
		{
			name:     "nil",
			expected: nil,
			actual:   nil,
		},
		{
			name:     "unsupported type",
			expected: make(chan int),
			result:   "expected: input must be of type string, []byte, or io.Reader",
		},
		{
			name:     "snapshot",
			expected: Snapshot(t),
			actual:   `[1,2,3]`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := DiffJSON(test.expected, test.actual)
			var resultText string
			if result != nil {
				resultText = result.String()
			}
			if resultText != test.result {
				t.Errorf("Unexpected result:\n%s\n", resultText)
			}
		})
	}
}

func TestInterface(t *testing.T) {
	tests := []struct {
		name             string
		expected, actual interface{}
		result           string
	}{
		{
			name:     "equal",
			expected: []string{"foo", "bar"},
			actual:   []string{"foo", "bar"},
		},
		{
			name:     "different",
			expected: []string{"foo", "bar"},
			actual:   []string{"bar", "bar"},
			result: `--- expected
+++ actual
@@ -1,4 +1,4 @@
 ([]string) (len=2) {
-  (string) (len=3) "foo",
+  (string) (len=3) "bar",
   (string) (len=3) "bar"
 }
`,
		},
	}
	for _, test := range tests {
		result := DiffInterface(test.expected, test.actual)
		var resultText string
		if result != nil {
			resultText = result.String()
		}
		if resultText != test.result {
			t.Errorf("Unexpected result:\n%s\n", resultText)
		}
	}
}
