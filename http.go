package testy

import (
	"bufio"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
)

// ResponseHandler wraps an existing http.Response, to be served as a
// standard http.Handler
type ResponseHandler struct {
	*http.Response
}

var _ http.Handler = &ResponseHandler{}

func (h *ResponseHandler) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	for header, values := range h.Header {
		for _, value := range values {
			w.Header().Add(header, value)
		}
	}
	if h.StatusCode != 0 {
		w.WriteHeader(h.StatusCode)
	}
	if h.Body != nil {
		defer h.Body.Close() // nolint: errcheck
		_, _ = io.Copy(w, h.Body)
	}
}

// ServeResponse starts a test HTTP server that serves r.
func ServeResponse(r *http.Response) *httptest.Server {
	return httptest.NewServer(&ResponseHandler{r})
}

// RequestValidator is a function that takes a *http.Request for validation.
type RequestValidator func(*testing.T, *http.Request)

// ValidateRequest returns a middleware that calls fn(), to validate the HTTP
// request, before continuing. An error returned by fn() will result in the
// addition of an X-Error header, a 400 status, and the error added to the
// body of the response.
func ValidateRequest(t *testing.T, fn RequestValidator) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			fn(t, r)
			next.ServeHTTP(w, r)
		})
	}
}

// ServeResponseValidator wraps a ResponseHandler with ValidateRequest
// middleware for a complete response-serving, request-validating test server.
func ServeResponseValidator(t *testing.T, r *http.Response, fn RequestValidator) *httptest.Server {
	mw := ValidateRequest(t, fn)
	return httptest.NewServer(mw(&ResponseHandler{r}))
}

// HTTPResponder is a function which intercepts and responds to an HTTP request.
type HTTPResponder func(*http.Request) (*http.Response, error)

var _ http.RoundTripper = HTTPResponder(nil)

// RoundTrip satisfies the http.RoundTripper interface
func (t HTTPResponder) RoundTrip(r *http.Request) (*http.Response, error) {
	return t(r)
}

// HTTPClient returns a customized *http.Client, which passes the request to
// fn, rather than to the network.
func HTTPClient(fn HTTPResponder) *http.Client { // nolint: interfacer
	return &http.Client{Transport: fn}
}

// ReadHTTPResponseFromFile reads an http.Response from a file. The req
// parameter optionally specifies the Request that corresponds to this
// Response. If nil, a GET request is assumed.
func ReadHTTPResponseFromFile(filename string, req *http.Request) (*http.Response, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close() // nolint: errcheck
	return http.ReadResponse(bufio.NewReader(f), req)
}

// JSONReader returns an io.ReadCloser that will produce i marshaled as JSON.
// Any marshaling error will be returned as an error to Read().
func JSONReader(i interface{}) io.ReadCloser {
	r, w := io.Pipe()
	go func() {
		err := json.NewEncoder(w).Encode(i)
		_ = w.CloseWithError(err)
	}()
	return r
}

// JSONRequest returns an HTTP request with i converted to JSON as the body,
// and the Content-Type header set to application/json.
func JSONRequest(method, url string, i interface{}) *http.Request {
	req := httptest.NewRequest(method, url, JSONReader(i))
	req.Header.Set("Content-Type", "application/json")
	return req
}

// QueryReader returns an io.ReadCloser that will produce the encoded value of
// v.
func QueryReader(v url.Values) io.ReadCloser {
	return io.NopCloser(strings.NewReader(v.Encode()))
}

// FormRequest returns an HTTP request with v converted to url-encoded values
// as the body, and the Content-Type header set to
// application/x-www-form-urlencoded.
func FormRequest(method, url string, v url.Values) *http.Request {
	req := httptest.NewRequest(method, url, QueryReader(v))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return req
}
