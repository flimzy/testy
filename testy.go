package testy

import (
	"fmt"
	"net/http"
	"regexp"
	"testing"

	"github.com/pkg/errors"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func stack(err error) string {
	if err, ok := err.(stackTracer); ok {
		stack := "\n"
		for _, f := range err.StackTrace() {
			stack += fmt.Sprintf("%+s:%d\n", f, f)
		}
		return stack
	}
	return ""
}

type statusCoder interface {
	StatusCode() int
}

type httpStatuser interface {
	HTTPStatus() int
}

type causer interface {
	Cause() error
}

// StatusCode returns the HTTP status code embedded in the error, or
// 500 if there is no specific status code.
//
// It will unwrap errors, according to pkg/errors and Go 1.13 Unwrap
// conventions, looking for an error with an embedded status code.
//
// To embed an HTTP status code, implement an error that satisfies one of the
// following interfaces, which are checked in this order:
//
//	 interface {
//	     HTTPStatus() int
//	}
//
//	 interface {
//	     StatusCode() int
//	}
func StatusCode(err error) int {
	if err == nil {
		return 0
	}
	var coder statusCoder
	var httpCoder httpStatuser
	for {
		if errors.As(err, &httpCoder) {
			return httpCoder.HTTPStatus()
		}
		if errors.As(err, &coder) {
			return coder.StatusCode()
		}
		if uw := errors.Unwrap(err); uw != nil {
			err = uw
			continue
		}
		if c, ok := err.(causer); ok {
			err = c.Cause()
			continue
		}
		return http.StatusInternalServerError
	}
}

func errorRE(expected string, actual error) error {
	var err string
	if actual != nil {
		err = actual.Error()
	}
	if (expected == "" && err != "") || (expected != "" && !regexp.MustCompile(expected).MatchString(err)) {
		return fmt.Errorf("Unexpected error: %s (expected /%s/)%s", err, expected, stack(actual))
	}
	return nil
}

type exitStatuser interface {
	ExitStatus() int
}

// ExitStatus returns the exit status embedded in the error, or 1 (unknown
// error) if there is no specific status code.
func ExitStatus(err error) int {
	if err == nil {
		return 0
	}
	if coder, ok := err.(exitStatuser); ok {
		return coder.ExitStatus()
	}
	return 1
}

// ExitStatusError compares actual.Error() and the embedded exit status against
// expected, and triggers an error if they do not match. If actual is non-nil,
// t.SkipNow() is called as well.
func ExitStatusError(t *testing.T, expected string, eStatus int, actual error) {
	t.Helper()
	var err string
	var actualEStatus int
	if actual != nil {
		err = actual.Error()
		actualEStatus = ExitStatus(actual)
	}
	if expected != err {
		t.Errorf("Unexpected error: %s (expected %s)", err, expected)
	}
	if eStatus != actualEStatus {
		t.Errorf("Unexpected exit status: %d (expected %d)", actualEStatus, eStatus)
	}
	if actual != nil {
		t.SkipNow()
	}
}

// ExitStatusErrorRE compares actual.Error() and the embedded exit status against
// expected, and triggers an error if they do not match. If actual is non-nil,
// t.SkipNow() is called as well.
func ExitStatusErrorRE(t *testing.T, expected string, eStatus int, actual error) {
	t.Helper()
	var err string
	var actualEStatus int
	if actual != nil {
		err = actual.Error()
		actualEStatus = ExitStatus(actual)
	}
	if (expected == "" && err != "") || (expected != "" && !regexp.MustCompile(expected).MatchString(err)) {
		t.Errorf("Unexpected error: %s (expected %s)", err, expected)
	}
	if actualEStatus != eStatus {
		t.Errorf("Unexpected exit status: %d (expected %d)", actualEStatus, eStatus)
	}
	if actual != nil {
		t.SkipNow()
	}
}

// FullError compares actual.Error() and the embedded HTTP and exit statuses
// against expected, and triggers an error if they do not match. If actual is
// non-nil, t.SkipNow() is called as well.
func FullError(t *testing.T, expected string, status, eStatus int, actual error) {
	t.Helper()
	var err string
	var actualStatus, actualEStatus int
	if actual != nil {
		err = actual.Error()
		actualStatus = StatusCode(actual)
		actualEStatus = ExitStatus(actual)
	}
	if expected != err {
		t.Errorf("Unexpected error: %s (expected %s)", err, expected)
	}
	if status != actualStatus {
		t.Errorf("Unexpected exit status: %d (expected %d)", actualStatus, status)
	}
	if eStatus != actualEStatus {
		t.Errorf("Unexpected exit status: %d (expected %d)", actualEStatus, eStatus)
	}
	if actual != nil {
		t.SkipNow()
	}
}

var stubRE = regexp.MustCompile(`[\s\/]`)

// Stub returns t.Name(), with whitespace and slashes converted to underscores.
func Stub(t *testing.T) string {
	// This to handle old versions of Go before the .Name() method was added.
	if n, ok := interface{}(t).(namer); ok {
		return stubRE.ReplaceAllString(n.Name(), "_")
	}
	t.Fatal("t.Name() not supported by your version of Go")
	return ""
}
