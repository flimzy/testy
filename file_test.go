package testy

import (
	"io"
	"os"
	"testing"
)

func TestFile(t *testing.T) {
	tests := []struct {
		name     string
		path     string
		expected string
		err      string
	}{
		{
			name: "not found",
			path: "./not_found",
			err:  "",
		},
		{
			name:     "found",
			path:     "testdata/test.txt",
			expected: "Test Content\n",
		},
	}
	for _, test := range tests {
		content, err := io.ReadAll(&File{Path: test.path})
		Error(t, test.err, err)
		if test.expected != string(content) {
			t.Errorf("Unexpected content: %s\n", string(content))
		}
	}
}

func TestSnapshot(t *testing.T) {
	t.Run("Test:Name:With:Colon", func(t *testing.T) {
		file := Snapshot(t)
		content, err := io.ReadAll(file)
		Error(t, "", err)
		if "test content\n" != string(content) {
			t.Errorf("Unexpected content: %s\n", string(content))
		}
	})
}

func setenv(t *testing.T, key, value string) { //nolint:unparam // general-purpose function that just isn't used much
	orig := os.Getenv(key)
	if err := os.Setenv(key, value); err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		if err := os.Setenv(key, orig); err != nil {
			t.Fatal(err)
		}
	})
}

func TestUpdateMode(t *testing.T) {
	t.Run("no env", func(t *testing.T) {
		setenv(t, "UPDATE_TESTY", "")
		if m := UpdateMode(); m {
			t.Errorf("Unexpected update mode: %v", m)
		}
	})
	t.Run("envUpdate", func(t *testing.T) {
		envUpdate = true
		t.Cleanup(func() {
			envUpdate = false
		})
		if m := UpdateMode(); !m {
			t.Errorf("Unexpected update mode: %v", m)
		}
	})
}

func Test_parseBool(t *testing.T) {
	tests := map[string]bool{
		"1":     true,
		"true":  true,
		"TRUE":  true,
		"TrUe":  true,
		"yES":   true,
		"false": false,
		"0":     false,
	}

	for input, want := range tests {
		t.Run(input, func(t *testing.T) {
			got := parseBool(input)
			if got != want {
				t.Errorf("Unexpected result: %v", got)
			}
		})
	}
}
