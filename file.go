package testy

import (
	"io"
	"os"
	"strings"
	"testing"
)

// File converts a file into an io.Reader.
// When UpdateMode is true, a detected difference will cause File to be
// overwritten with the actual value, when File is the expected value.
type File struct {
	Path string
	r    io.Reader
	done bool
}

var _ io.Reader = &File{}

// Snapshot returns a File object suitable for storing test output. It is
// essentially shorthand for:
//
//	&File{Path: "testdata/"+Stub(t)}
//
// but with colons in the stub converted to underscores, for better
// compatibility certain tools.
//
// The optional suffix argument(s) are appended to the generated name. This is
// useful when multiple snapshots are used per test.
func Snapshot(t *testing.T, suffix ...string) *File {
	parts := append([]string{Stub(t)}, suffix...)
	return &File{
		Path: "testdata/" + strings.ReplaceAll(strings.Join(parts, "_"), ":", "_"),
	}
}

func (f *File) Read(p []byte) (int, error) {
	if f.done {
		return 0, io.EOF
	}
	if f.r == nil {
		var err error
		if f.r, err = os.Open(f.Path); err != nil {
			if os.IsNotExist(err) {
				f.done = true
				return 0, io.EOF
			}
			return 0, err
		}
	}
	n, err := f.r.Read(p)
	f.done = err != nil
	return n, err
}

// envUpdate is read once on startup, to prevent modification of env variables
// during tests from interfering
var envUpdate = parseBool(os.Getenv("UPDATE_TESTY"))

func parseBool(input string) bool {
	switch strings.ToLower(input) {
	case "true", "1", "yes":
		return true
	}
	return false
}

// UpdateMode returns true if update mode is enabled, which indicates whether
// a detected diff should cause a File to be overwritten. To enable UpdateMode,
// run `go test -tag=update` or `go test -update`.
func UpdateMode() bool {
	if envUpdate {
		return true
	}
	return *updateMode
}
