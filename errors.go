package testy

import "regexp"

// ErrorMatches returns true if want matches got's error text, or if want is
// empty and got is nil.
func ErrorMatches(want string, got error) bool {
	if got != nil {
		return want == got.Error()
	}
	return want == ""
}

// ErrorMatchesRE returns true if want matches got's error text. As a special
// case, want=="" matches got==nil
func ErrorMatchesRE(want string, got error) bool {
	if got == nil {
		return want == ""
	}
	if want == "" {
		return false
	}
	return regexp.MustCompile(want).MatchString(got.Error())
}
