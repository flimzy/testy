package testy

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestServeResponse(t *testing.T) {
	type tt struct {
		response *http.Response
		expected *http.Response
	}

	tests := NewTable()

	tests.Add("Simple GET response", tt{
		response: &http.Response{
			Header: http.Header{
				"X-Foo": []string{"foo"},
				"Date":  []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
			},
		},
		expected: &http.Response{
			StatusCode: 200,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"X-Foo":          []string{"foo"},
				"Date":           []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
				"Content-Length": []string{"0"},
			},
		},
	})
	tests.Add("Simple response with body", func(t *testing.T) interface{} {
		SkipVer(t, ">= 1.16")
		return tt{
			response: &http.Response{
				StatusCode: 200,
				Header: http.Header{
					"Date": []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
				},
				Body: io.NopCloser(strings.NewReader("the body\nof the response\n")),
			},
			expected: &http.Response{
				StatusCode:    200,
				ProtoMajor:    1,
				ProtoMinor:    1,
				ContentLength: 25,
				Header: http.Header{
					"Date":         []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
					"Content-Type": []string{"text/plain; charset=utf-8"},
				},
				Body: io.NopCloser(strings.NewReader("the body\nof the response\n")),
			},
		}
	})
	tests.Add("Simple response with body, 1.16", func(t *testing.T) interface{} {
		SkipVer(t, "< 1.16")
		return tt{
			response: &http.Response{
				StatusCode: 200,
				Header: http.Header{
					"Content-Length": []string{"25"},
					"Date":           []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
				},
				Body: io.NopCloser(strings.NewReader("the body\nof the response\n")),
			},
			expected: &http.Response{
				StatusCode:    200,
				ProtoMajor:    1,
				ProtoMinor:    1,
				ContentLength: 25,
				Header: http.Header{
					"Date":         []string{"Tue, 07 Aug 2018 20:18:51 GMT"},
					"Content-Type": []string{"text/plain; charset=utf-8"},
				},
				Body: io.NopCloser(strings.NewReader("the body\nof the response\n")),
			},
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		s := httptest.NewServer(&ResponseHandler{tt.response})
		defer s.Close()
		res, err := http.Get(s.URL)
		if err != nil {
			t.Fatal(err)
		}
		if d := DiffHTTPResponse(tt.expected, res); d != nil {
			t.Error(d)
		}
	})
}
