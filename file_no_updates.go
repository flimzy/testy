//go:build !update
// +build !update

package testy

import "flag"

var updateMode = flag.Bool("update-testy", false, "update golden/snapshot files")
