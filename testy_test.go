package testy

import (
	"errors"
	"testing"
)

func Test_errorRE(t *testing.T) {
	t.Run("no error expected, but we got one", func(t *testing.T) {
		err := errors.New("this is an error")
		ex := errorRE("", err)
		if ex == nil {
			t.Errorf("expected errorRE to fail")
		}
	})
	t.Run("error expected, but not found", func(t *testing.T) {
		ex := errorRE("foo", nil)
		if ex == nil {
			t.Errorf("expected errorRE to fail")
		}
	})
	t.Run("no error expected, and none found", func(t *testing.T) {
		ex := errorRE("", nil)
		if ex != nil {
			t.Errorf("expected errorRE to pass")
		}
	})
	t.Run("different error than expected", func(t *testing.T) {
		ex := errorRE("foo", errors.New("bar"))
		Error(t, "Unexpected error: bar (expected /foo/)", ex)
	})
	t.Run("got the expected error", func(t *testing.T) {
		ex := errorRE("foo", errors.New("foo"))
		if ex != nil {
			t.Errorf("expected errorRE to pass")
		}
	})
}
