## Testy

You are browsing the **development** branch of this project. This branch will eventually become a **v2**, but for now it is unstable, and expected to change in wild and unpredictable ways until further notice. You may have better luck using the [stable version](https://pkg.go.dev/gitlab.com/flimzy/testy) for now.

Testy is a collection of utilities to facilitate unit testing in Go.

This software is released under the MIT license, as outlined in the included
LICENSE.md file.
