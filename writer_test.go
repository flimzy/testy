package testy

import (
	"errors"
	"fmt"
	"io"
	"testing"
	"time"
)

// Error compares actual.Error() against expected, and triggers an error if
// they do not match. If actual is non-nil, t.SkipNow() is called as well.
//
// Deprecated: Do not use this function. Consider using [ErrorMatches] instead.
func Error(t *testing.T, expected string, actual error) {
	t.Helper()
	var err string
	if actual != nil {
		err = actual.Error()
	}
	if expected != err {
		t.Errorf("Unexpected error: %s (expected %s)%s", err, expected, stack(actual))
	}
	if actual != nil {
		t.SkipNow()
	}
}

func TestErrWriter(t *testing.T) {
	t.Run("0 bytes", func(t *testing.T) {
		w := ErrorWriter(0, errors.New("foo err"))
		_, err := fmt.Fprintln(w, "foo")
		Error(t, "foo err", err)
	})
	t.Run("5 bytes", func(t *testing.T) {
		w := ErrorWriter(5, errors.New("foo err"))
		c, err := fmt.Fprintln(w, "this is more than 5 bytes")
		if c != 5 {
			t.Errorf("Expected to write 5 bytes, wrote %d", c)
		}
		Error(t, "foo err", err)
	})
	t.Run("two writes", func(t *testing.T) {
		w := ErrorWriter(5, errors.New("foo err"))
		_, err := fmt.Fprintln(w, "xx")
		if err != nil {
			t.Errorf("Expected no error yet, got: %s", err)
		}
		c, err := fmt.Fprintln(w, "this is more than 5 bytes")
		if c != 2 {
			t.Errorf("Expected to write 5 bytes, wrote %d", c)
		}
		Error(t, "foo err", err)
	})
}

func TestErrReader(t *testing.T) {
	t.Run("0 bytes", func(t *testing.T) {
		r := ErrorReader("", errors.New("foo err"))
		_, err := io.ReadAll(r)
		Error(t, "foo err", err)
	})
	t.Run("buffer", func(t *testing.T) {
		r := ErrorReader("foo text", errors.New("foo err"))
		c, err := io.ReadAll(r)
		Error(t, "foo err", err)
		if string(c) != "foo text" {
			t.Errorf("Unexpected data returned: %s", string(c))
		}
	})
}

func TestRecorder(t *testing.T) {
	const expected = "test"

	t.Run("write then read", func(t *testing.T) {
		rec := Recorder()
		if _, err := fmt.Fprint(rec, expected); err != nil {
			t.Fatal(err)
		}
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		result, err := io.ReadAll(rec)
		if err != nil {
			t.Fatal(err)
		}
		if string(result) != expected {
			t.Errorf("Unexpected result: %s", string(result))
		}
	})
	t.Run("write after close", func(t *testing.T) {
		rec := Recorder()
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		_, err := fmt.Fprint(rec, "xxx")
		Error(t, "recorder closed for writing", err)
	})
	t.Run("read blocks until closed", func(t *testing.T) {
		rec := Recorder()
		if _, err := fmt.Fprint(rec, expected); err != nil {
			t.Fatal(err)
		}
		go func() {
			time.Sleep(200 * time.Millisecond)
			if err := rec.Close(); err != nil {
				panic(err)
			}
		}()
		start := time.Now()
		result, err := io.ReadAll(rec)
		if err != nil {
			t.Fatal(err)
		}
		elapsed := time.Since(start)
		if elapsed < 150*time.Millisecond {
			t.Errorf("read should take ~200ms, only took %v", elapsed)
		}
		if string(result) != expected {
			t.Errorf("Unexpected result: %s", string(result))
		}
	})
	t.Run("read after close", func(t *testing.T) {
		rec := Recorder()
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		_, err := io.ReadAll(rec)
		Error(t, "recorder closed", err)
	})
	t.Run("fully closed", func(t *testing.T) {
		rec := Recorder()
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		if err := rec.Close(); err != nil {
			t.Fatal(err)
		}
		err := rec.Close()
		Error(t, "already closed", err)
	})
}
